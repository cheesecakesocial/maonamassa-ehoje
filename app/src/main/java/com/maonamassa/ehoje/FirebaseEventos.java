package com.maonamassa.ehoje;


import java.util.ArrayList;
import java.util.HashMap;

public class FirebaseEventos {
    private String eventName;
    private String eventDescription;
    private HashMap<String,Boolean> confirmados=new HashMap<>();
    private HashMap<String,Boolean> convidados=new HashMap<>();
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public HashMap<String, Boolean> getConfirmados() {
        return confirmados;
    }

    public void setConfirmados(HashMap<String, Boolean> confirmados) {
        this.confirmados = confirmados;
    }

    public HashMap<String, Boolean> getConvidados() {
        return convidados;
    }

    public void setConvidados(HashMap<String, Boolean> convidados) {
        this.convidados = convidados;
    }
}
