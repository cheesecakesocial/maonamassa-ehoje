package com.maonamassa.ehoje;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.*;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class CriaEventoActivity extends AppCompatActivity {

    private Button createEvent;
    private Button locateEvent;
    private EditText eventName;
    private EditText eventDescription;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cria_evento);

        createEvent = (Button) findViewById(R.id.buttonCreateEvent);
        //locateEvent = (Button) findViewById(R.id.buttonLocate);
        eventName = (EditText) findViewById(R.id.eventName);
        eventDescription = (EditText) findViewById(R.id.descriptionEvent);

        createEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createEvent();
            }
        });
    }


    public void locEvent(View view) {

        // Como iniciar uma nova Activity ou "ir para outra tela".
        Intent intent = new Intent(this, DetalhesEventosActivity.class);
        startActivity(intent);


    }

    public void createEvent() {


        if(eventName.getText().length() == 0 || eventDescription.getText().length() == 0) {
            Toast.makeText(getApplication(), " Os campos nome e descrição são obrigatórios",
                    Toast.LENGTH_LONG).show();

        }else {
            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("eventos");
            final String key = EHojeApplication.getInstance().getUserid();

            final FirebaseEventos evento = new FirebaseEventos();
            evento.setEventName(eventName.getText().toString());
            evento.setEventDescription(eventDescription.getText().toString());
            evento.getConfirmados().put(key,true);


            databaseReference.child(key).setValue(evento).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    String success = task.isSuccessful() ? "success" : "failure";


                    FirebaseUser user=EHojeApplication.getInstance().getUser();
                    user.setEventoId(key);
                    user.setEventoName(evento.getEventName());

                    FirebaseDatabase.getInstance().getReference().child("users").child(key).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            finish();

                            Toast.makeText(getApplication(), " Evento " +eventName.getText().toString()+ " Criado com sucesso !",
                                    Toast.LENGTH_LONG).show();

                        }
                    });




                }
            });

        }
    }
}
