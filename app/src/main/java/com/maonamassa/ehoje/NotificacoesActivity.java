package com.maonamassa.ehoje;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;

import static com.maonamassa.ehoje.R.id.recyclerViewNotificacoes;

public class NotificacoesActivity extends AppCompatActivity {


    RecyclerView recyclerView;
    EventsAdapter adapter = new EventsAdapter();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificacoes);
        recyclerView = (RecyclerView) findViewById(recyclerViewNotificacoes);
        recyclerView.setAdapter(adapter);
        fetchData();
    }

   void fetchData() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("eventos");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (!dataSnapshot.exists()) {
                    return;
                }
                Iterator<DataSnapshot> iterator = dataSnapshot.getChildren()
                        .iterator();
                ArrayList<FirebaseEventos> list = new ArrayList<>();
                while (iterator.hasNext()) {
                    DataSnapshot next=iterator.next();
                    FirebaseEventos data = next.getValue(FirebaseEventos.class);
                    data.setId(next.getKey());
                    if (data.getConvidados().containsKey(EHojeApplication.getInstance().getUserid())){
                        list.add(data);
                    }

                }
                adapter.events= list;
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    public void confirmEvent(View wiew){

        alertFailure();

    }


    private void alertFailure() {
        AlertDialog falhaLogin;
        AlertDialog.Builder alerta = new AlertDialog.Builder(NotificacoesActivity.this);
        alerta.setTitle("Todo ");
        alerta.setMessage("Implementar metodo");

        falhaLogin = alerta.create();
        falhaLogin.show();
    }






}
