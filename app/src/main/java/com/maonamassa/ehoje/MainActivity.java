package com.maonamassa.ehoje;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.content.Intent;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    Button eventButton;
    Button notificationButton;
    Button contactsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        eventButton = (Button) findViewById(R.id.buttonEventos);
        notificationButton = (Button) findViewById(R.id.buttonNotificacoes);
        contactsButton = (Button) findViewById(R.id.buttonContatos);

    }

     public void eventClicked(View view){

         // Como iniciar uma nova Activity ou "ir para outra tela".
         Intent intent = new Intent(this, CriaEventoActivity.class);
         startActivity(intent);
     }

     public void notificationClicked(View view){
         // Como iniciar uma nova Activity ou "ir para outra tela".
         Intent intent = new Intent(this, NotificacoesActivity.class);
         startActivity(intent);

     }

     public void contactClicked(View view){
         // Como iniciar uma nova Activity ou "ir para outra tela".
         Intent intent = new Intent(this, ContatosActivity.class);
         startActivity(intent);


     }








}







