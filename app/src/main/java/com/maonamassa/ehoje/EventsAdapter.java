package com.maonamassa.ehoje;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;

import static com.maonamassa.ehoje.R.styleable.RecyclerView;

public class EventsAdapter extends RecyclerView.Adapter<EventViewHolder> {

    public ArrayList<FirebaseEventos> events = new ArrayList<FirebaseEventos>();

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_notificacoes, parent, false);
        return new EventViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        FirebaseEventos event = events.get(position);
        holder.setEvent(event,events);
    }

    @Override
    public int getItemCount() {
        return events.size();
    }


}

class EventViewHolder extends RecyclerView.ViewHolder {

    TextView nameTextView;
    TextView userTextView;
    Button confirmButton;

    public EventViewHolder(View itemView) {
        super(itemView);

        userTextView = ( TextView) itemView.findViewById(R.id.item_user);
        nameTextView = (TextView) itemView.findViewById(R.id.item_event);
        confirmButton = (Button) itemView.findViewById(R.id.confirmEvent);
    }

    public void setEvent(final FirebaseEventos event,final ArrayList<FirebaseEventos> events) {
        nameTextView.setText(event.getEventName());
        userTextView.setText(event.getEventDescription());
        confirmButton.setEnabled(!event.getConfirmados().containsKey(EHojeApplication.getInstance().getUserid()));
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String userKey = EHojeApplication.getInstance().getUserid();
                FirebaseUser user = EHojeApplication.getInstance().getUser();
                if(user.getEventoId()!=null && user.getEventoId().equals(userKey)){
                    for (FirebaseEventos e : events){
                        if (e.getId().equals(user.getEventoId())){
                            String[] usersIds = e.getConfirmados().keySet().toArray(new String[e.getConfirmados().keySet().size()]);
                            for (String id: usersIds){
                                HashMap<String,Object> usermap = new HashMap<String, Object>();
                                usermap.put("eventoId","");
                                usermap.put("eventoName","");
                                FirebaseDatabase.getInstance().getReference().child("users").child(id).updateChildren(usermap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                    }
                                });
                            }
                            FirebaseDatabase.getInstance().getReference().child("eventos").child(e.getId()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                }
                            });
                        }
                    }
                }



                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("eventos").child(event.getId()).child("confirmados").child(userKey);
                databaseReference.setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            FirebaseUser user=EHojeApplication.getInstance().getUser();
                            user.setEventoId(userKey);
                            user.setEventoName(event.getEventName());

                            FirebaseDatabase.getInstance().getReference().child("users").child(userKey).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    Toast.makeText(EHojeApplication.getInstance(), "confirmado com sucesso", Toast.LENGTH_SHORT).show();

                                }
                            });
                        }
                    }
                });
            }
        });

    }


}
