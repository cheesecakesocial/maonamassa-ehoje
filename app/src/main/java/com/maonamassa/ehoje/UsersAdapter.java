package com.maonamassa.ehoje;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;

public class UsersAdapter extends RecyclerView.Adapter<UserViewHolder> {

    public ArrayList<FirebaseUser> users = new ArrayList<FirebaseUser>();

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_contatos, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        FirebaseUser user = users.get(position);
        holder.setUser(user);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }


}

class UserViewHolder extends RecyclerView.ViewHolder {

    TextView nameTextView;
    Button inviteButton;
    TextView eventTextView;

    public UserViewHolder(View itemView) {
        super(itemView);
        nameTextView = (TextView) itemView.findViewById(R.id.item_user);
        inviteButton = (Button) itemView.findViewById(R.id.InviteEvent);
        eventTextView = (TextView) itemView.findViewById(R.id.item_event);
    }

    public void setUser(final FirebaseUser user) {
        nameTextView.setText(user.getEmail());
        eventTextView.setText(user.getEventoName());
        inviteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String eventKey = EHojeApplication.getInstance().getUserid();
                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("eventos").child(eventKey).child("convidados").child(user.getId());
                databaseReference.setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            Toast.makeText(EHojeApplication.getInstance(), "convidado com sucesso", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }


}
