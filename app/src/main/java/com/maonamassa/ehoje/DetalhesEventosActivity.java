package com.maonamassa.ehoje;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;

import static com.maonamassa.ehoje.R.id.recyclerViewDetalhes;
import static com.maonamassa.ehoje.R.id.recyclerViewNotificacoes;

public class DetalhesEventosActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    DetalhesAdapter adapter = new DetalhesAdapter();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes_eventos);
        recyclerView = (RecyclerView) findViewById(recyclerViewDetalhes);
        recyclerView.setAdapter(adapter);
        fetchData();
    }



    void fetchData() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("eventos");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    return;
                }
                Iterator<DataSnapshot> iterator = dataSnapshot.getChildren()
                        .iterator();
                ArrayList<FirebaseEventos> list = new ArrayList<>();
                while (iterator.hasNext()) {
                    FirebaseEventos data = iterator.next().getValue(FirebaseEventos.class);
                    list.add(data);
                }
                adapter.events= list;
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }










}






