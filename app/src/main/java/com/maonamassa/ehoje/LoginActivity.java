package com.maonamassa.ehoje;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.*;
import com.google.firebase.database.FirebaseDatabase;

public class LoginActivity extends AppCompatActivity {


    private Button loginButton;
    private EditText user;
    private EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginButton = (Button) findViewById(R.id.buttonLogin);

        user = (EditText) findViewById(R.id.userLogin);
        password = (EditText) findViewById(R.id.passwordLogin);

    }


    public void loginClicked(View view) {
        String email = user.getText()
                .toString();
        String senha = password.getText()
                .toString();



        if(user.getText().length() == 0 || password.getText().length() == 0){
            Toast.makeText(getApplication(), " Os campos login e senha são obrigatórios",
                    Toast.LENGTH_LONG).show();
        }else if (email != null && senha != null) {
            FirebaseAuth.getInstance()
                    .signInWithEmailAndPassword(email, senha)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                String uid = task.getResult().getUser().getUid();
                                String email = task.getResult().getUser().getEmail();
                                saveUser(uid, email);

                                Toast.makeText(getApplication(), "Seja bem vindo ! "+user.getText().toString(),
                                        Toast.LENGTH_LONG).show();

                                user.setText("");
                                password.setText("");



                            } else {
                                alertFailure();
                            }
                        }
                    });
        }


    }

    private void saveUser(final String uid, final String email) {
        FirebaseDatabase.getInstance().getReference().child("users").child(uid).child("email").setValue(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Log.d("Add user", "is successful? " + task.isSuccessful());
                if (task.isSuccessful()) {
                    FirebaseUser user = new FirebaseUser();
                    user.setEmail(email);
                    EHojeApplication.getInstance().setUser(user);
                    EHojeApplication.getInstance().setUserid(uid);
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    alertFailure();
                }
            }
        });
    }

    private void alertFailure() {

        Toast.makeText(getApplication(), "Falha no Login, "+user.getText().toString()+" Tente novamente !",
                Toast.LENGTH_LONG).show();

    }

    public Button getLoginButton() {
        return loginButton;
    }

    public void setLoginButton(Button loginButton) {
        this.loginButton = loginButton;
    }

    public EditText getUser() {
        return user;
    }

    public void setUser(EditText user) {
        this.user = user;
    }

    public EditText getPassword() {
        return password;
    }

    public void setPassword(EditText password) {
        this.password = password;
    }


}
