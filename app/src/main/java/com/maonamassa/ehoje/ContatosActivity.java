package com.maonamassa.ehoje;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;

public class ContatosActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    UsersAdapter adapter = new UsersAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contatos);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewContatos);
        recyclerView.setAdapter(adapter);
        fetchData();
    }

    void fetchData() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("users");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    return;
                }
                Iterator<DataSnapshot> iterator = dataSnapshot.getChildren()
                        .iterator();
                ArrayList<FirebaseUser> list = new ArrayList<>();
                while (iterator.hasNext()) {
                    DataSnapshot next = iterator.next();
                    FirebaseUser data = next.getValue(FirebaseUser.class);
                    data.setId(next.getKey());
                    list.add(data);
                }
                adapter.users = list;
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    public void InviteEvent(View wiew){



    }


    private void alertFailure() {
        AlertDialog falhaLogin;
        AlertDialog.Builder alerta = new AlertDialog.Builder(ContatosActivity.this);
        alerta.setTitle("Todo ");
        alerta.setMessage("Implementar metodo");

        falhaLogin = alerta.create();
        falhaLogin.show();
    }



}




