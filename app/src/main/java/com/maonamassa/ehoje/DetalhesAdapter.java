package com.maonamassa.ehoje;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import static com.maonamassa.ehoje.R.layout.item_detalhes;

/**
 * Created by lb on 23/06/17.
 */

public class DetalhesAdapter  extends RecyclerView.Adapter<EventViewHolder> {

    public ArrayList<FirebaseEventos> events = new ArrayList<FirebaseEventos>();


    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(item_detalhes, parent, false);
        return new EventViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        FirebaseEventos event = events.get(position);
//        holder.setEvent(event);

    }

    @Override
    public int getItemCount() {
        return events.size();
    }

}
class DetalhesViewHolder extends RecyclerView.ViewHolder {


    TextView userTextView;
    TextView detalheTextView;


    public DetalhesViewHolder(View itemView) {
        super(itemView);

        userTextView = (TextView) itemView.findViewById(R.id.item_user);
        detalheTextView = (TextView) itemView.findViewById(R.id.item_event);

    }

    public void setEvent(FirebaseEventos event) {
         detalheTextView.setText(event.getEventDescription());
         userTextView.setText(event.getEventName());

    }
}
